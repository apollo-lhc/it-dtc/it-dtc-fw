library ieee;
use ieee.std_logic_1164.all;

Library UNISIM;
use UNISIM.vcomponents.all;

use work.axiRegPkg.all;
use work.axiRegPkg_d64.all;
use work.types.all;
use work.AXISlaveAddrPkg.all;

use work.ITDTC_TCDS_Ctrl.all;


entity FAKE_TCDS is
  generic (
    ALLOCATED_MEMORY_RANGE : integer;
    FREERUN_FREQ : integer := 50000000
    );
  port (
    freerun_clk     : in std_logic;
    freerun_rst     : in std_logic;
    TCDS_BP_clk_p   : in std_logic;
    TCDS_BP_clk_n   : in std_logic;
    locked_320Mhz   : out std_logic;
    clk_320Mhz      : out std_logic;
    --these 40 aren't really aligned.  If it matters, time to use real TCDS2
    clk_320en40     : out std_logic;
    clk_40Mhz_out_p : out std_logic;
    clk_40Mhz_out_n : out std_logic;
    --
    clk_axi          : in  std_logic;
    reset_axi_n      : in  std_logic;
    slave_readMOSI   : in  AXIReadMOSI;
    slave_readMISO   : out AXIReadMISO  := DefaultAXIReadMISO;
    slave_writeMOSI  : in  AXIWriteMOSI;
    slave_writeMISO  : out AXIWriteMISO := DefaultAXIWriteMISO
    );
end entity FAKE_TCDS;

architecture behavioral of FAKE_TCDS is
  signal refclk_TCDS_BP     : std_logic;
  signal refclk_TCDS_BP_int : std_logic;
  signal clk_320_int        : std_logic;
  signal clk_40Mhz_FB       : std_logic;

  signal clk_40rec          : std_logic;
  
  signal locked             : std_logic;
  signal clk_en_sr          : std_logic_vector(7 downto 0);

  signal freq_320Mhz        : std_logic_vector(31 downto 0);
  signal freq_40ce          : std_logic_vector(31 downto 0);
  
  signal Mon                : ITDTC_TCDS_Mon_t;
  
  component FAKE_TCDS is
    port (
      clk_320_63176MHz       : out STD_LOGIC;
      clk_40_0775MHz         : out STD_LOGIC;
      reset                  : in  STD_LOGIC;
      locked                 : out STD_LOGIC;
      FAKE_TCDS_320_63176MHz : in  STD_LOGIC);
  end component FAKE_TCDS;

begin

  --Capture the backplane clock
  --fake TCDS capture emulates the TCLINK CDR
  ibufds_TCDS_BP : ibufds_gte4
    generic map (
      REFCLK_EN_TX_PATH  => '0',
      REFCLK_HROW_CK_SEL => "00",
      REFCLK_ICNTL_RX    => "00")
    port map (
      O     => open,
      ODIV2 => refclk_TCDS_BP_int,
      CEB   => '0',
      I     => TCDS_BP_clk_p,
      IB    => TCDS_BP_clk_n
      );
  --put this clock on the fabric clock routing
  BUFG_GT_TCDS_BP : BUFG_GT
    port map (
      O => refclk_TCDS_BP,
      CE => '1',
      CEMASK => '1',
      CLR => '0',
      CLRMASK => '1', 
      DIV => "000",
      I => refclk_TCDS_BP_int
      );
  --Run this clock through a MMCM with phase alignment. 
  FAKE_TCDS_1: entity work.FAKE_TCDS_clk
    port map (
      clk_320_63176MHz       => clk_320_int,
      clk_40_07897MHz         => clk_40Mhz_FB,
      reset                  => '0',
      locked                 => locked,
      FAKE_TCDS_clk_320_63176MHz => refclk_TCDS_BP);
  clk_320Mhz <= clk_320_int;
  
  --Capture the rate of the 320 clock
  rate_counter_TCDS_ref: entity work.rate_counter
    generic map (
      CLK_A_1_SECOND => FREERUN_FREQ)
    port map (
      clk_A         => freerun_clk,
      clk_B         => clk_320_int,
      reset_A_async => freerun_rst,
      event_b       => '1',
      rate          => freq_320Mhz
      );            

  --Capture the rate of the 40CE signal
  rate_counter_TCDS_40ce: entity work.rate_counter
    generic map (
      CLK_A_1_SECOND => FREERUN_FREQ)
    port map (
      clk_A         => freerun_clk,
      clk_B         => clk_320_int,
      reset_A_async => freerun_rst,
      event_b       => clk_en_sr(0),
      rate          => freq_40ce
      );            

  
  Mon.FREQ_320 <= freq_320Mhz;
  Mon.FREQ_40CE <= freq_40CE;
  Mon.STATUS.LOCKED <= locked;
  
  --Generate the clkEN and the FB clock
  clk_320en40 <= clk_en_sr(0);
  clk_40_gen: process (refclk_TCDS_BP, locked) is
  begin  -- process clk_40_gen
    if locked = '0' then               -- asynchronous reset (active low)
      clk_en_sr <= "10000000";
    elsif refclk_TCDS_BP'event and refclk_TCDS_BP = '1' then  -- rising clock edge
      --shift register for clock enable 
      clk_en_sr <= clk_en_sr(0) & clk_en_sr(7 downto 1);
    end if;
  end process clk_40_gen;


  clk_40Rec_ddr: oddre1
    port map (
      C => clk_40Mhz_FB,
      D1 => '1',
      D2 => '0',
      Q => CLK_40Rec,
      SR => '0'
      );
      
  clk_40Rec_output: obufds
    port map (
      i  => CLK_40Rec,
      o  => clk_40Mhz_out_p,
      ob => clk_40Mhz_out_n
      );  


  F1_ITDTC_TCDS_map_1: entity work.ITDTC_TCDS_map
    generic map (
      ALLOCATED_MEMORY_RANGE => ALLOCATED_MEMORY_RANGE)
    port map (
      clk_axi         => clk_axi,
      reset_axi_n     => reset_axi_n,
      slave_readMOSI  => slave_readMOSI,
      slave_readMISO  => slave_readMISO,
      slave_writeMOSI => slave_writeMOSI,
      slave_writeMISO => slave_writeMISO,
      Mon             => Mon);
  
end architecture behavioral;
