library ieee;
use ieee.std_logic_1164.all;

package body EventBuilder_PKG is

  constant CHIP_DATA_WIDTH : integer := 32;
  constant CHIP_META_WIDTH : integer := 9;  

  constant EB_CHIP_DATA_WIDTH : integer := 64;
  
  type chip_data_array_t is array (integer range <>) of std_logic_vector(CHIP_DATA_WIDTH -1 downto 0);
  type chip_meta_array_t is array (integer range <>) of std_logic_vector(CHIP_META_WIDTH -1 downto 0);

  type state_t is (SM_RESET,
                   SM_INIT,
                   SM_WAITING,
                   SM_READY,
                   SM_STREAMING,
                   SM_ERROR);

  
end package body EventBuilder_PKG;
