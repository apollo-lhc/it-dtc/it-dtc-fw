library ieee;
use ieee.std_logic_1164.all;
use work.types.all;

entity FragmentStreamer is
  
  port (
    clk                               : in  std_logic;
    reset                             : in  std_logic;
                                            
    stream_data_in                    : in  std_logic_vector(STREAM_DATA_BITS -1 downto 0);
    stream_data_in_dv                 : in  std_logic;    
    stream_start_in                   : in  std_logic;
    stream_end_in                     : in  std_logic;  -- must only be
                                                        -- asserted on a
                                                        -- stream_data_in_dv '1'
    stream_pos_in                     : in  slv6_t;
    ready_for_new_fragment            : out std_logic;
    stream_fragment                   : out std_logic;
    
    datastream_trigger_done           : out std_logic;
    datastream_trigger_valid          : in  std_logic;
    datastream_trigger_not_found      : in  std_logic;
    datastream_trigger_expect_no_data : in  std_logic;
    datastream_trigger_ID             : in  std_logic_vector(FE_BX_ID_SIZE-1+2 downto 0);
    datastream_trigger_DAQ_type       : in  std_logic;
    datastream_trigger_LUMI_type      : in  std_logic;
    
    eFIFO_DAQ_stream_data_out         : out std_logic_vector(64 downto 0);
    eFIFO_DAQ_stream_data_out_dv      : out std_logic;
    eFIFO_DAQ_stream_data_full        : in  std_logic;    
    eFIFO_DAQ_meta_data_out           : out meta_data_t;
    eFIFO_DAQ_meta_data_dv            : out std_logic;
    eFIFO_DAQ_meta_data_full          : in  std_logic

    eFIFO_LUMI_stream_data_out        : out std_logic_vector(64 downto 0);
    eFIFO_LUMI_stream_data_out_dv     : out std_logic;
    eFIFO_LUMI_stream_data_full       : in  std_logic;    
    eFIFO_LUMI_meta_data_out          : out meta_data_t;
    eFIFO_LUMI_meta_data_dv           : out std_logic;
    eFIFO_LUMI_meta_data_full         : in  std_logic

    );

end entity FragmentStreamer;


architecture behavioral of FragmentStreamer is

  --decoder signals
  signal stream_word_pos : slv6_t;
--  signal stream_start_pos_valid : std_logic;

  type SMFS_state is (SMFS_RESET,
                      SMFS_INIT,
                      SMFS_IDLE,
                      SMFS_GET_INFO,
                      SMFS_DROP_DATA,
                      SMFS_STREAM_EMPTY_FRAGMENT,
                      SMFS_STREAM,
                      SMFS_ERROR);      

  -- efifo signals
  signal efifo_words_written : slv_32_t;
  signal efifo_words_reset : std_logic;


  signal eFIFO_stream_data_out         :  std_logic_vector(64 downto 0);
  signal eFIFO_stream_data_out_dv      :  std_logic;
  signal eFIFO_meta_data_out           :  meta_data_t;
  signal eFIFO_meta_data_dv            :  std_logic;


  
begin  -- architecture behavioral

  -- State machien is busy unless it is in the idle state
  ready_for_new_fratment <= '1' when SMFS_state = SMFS_IDLE else '1';  
  state_machine: process (clk, reset) is
  begin  -- process state_machine
    if reset = '1' then                 -- asynchronous reset (active high)
      SMFS_state <= SMFS_RESET;
    elsif clk'event and clk = '1' then  -- rising clock edge
      case SMFS_state is
        when SMFS_RESET =>
          ---SMFS_RESET----------------------------------------------------------
          SMFS_state <= SMFS_INIT;
        when SMFS_INIT =>
          ---SMFS_INIT-----------------------------------------------------------
          SMFS_state <= SMFS_IDLE;
        when SMFS_IDLE =>
          ---SMFS_IDLE-----------------------------------------------------------
          if stream_start_in = '1'  then
            -- A new fragment is ready
            SMFS_state <= SMFS_GET_INFO;
          else
            SMFS_state <= SMFS_IDLE;
          end if;
        when SMFS_GET_INFO =>
          ---SMFS_GET_INFO-------------------------------------------------------
          if datastream_trigger_valid = '1' then
            --We have a response from the TriggerMap
            if datastream_trigger_not_found = '1' then
              --This tag wasn't expected.  Drop the data (probably capture for
              --debug)
              SMFS_state <= SMFS_DROP_DATA;
            elsif datastream_trigger_expect_no_data = '1' then
              -- This tag was for a newer bx, so we are passing on empty
              -- fragments for the missing tag/bxs first
              SMFS_state <=SMFS_STREAM_EMPTY_FRAGMENT;
            else
              -- We found a match and can stream our data. 
              SMFS_state <= SMFS_STREAM;
            end if;
          else
            SMFS_state <= SMFS_GET_INFO;
          end if;
        when SMFS_STREAM =>
          ---SMFS_STREAM---------------------------------------------------------
          if eFIFO_meta_data_full = '0' and meta_word = FM_SIZE_LSB_WORD then
            SMFS_State <= SMFS_IDLE;                       
          end if;
        when SMFS_DROP_DATA =>
          ---SMFS_DROP_DATA------------------------------------------------------
        when SMFS_STREAM_EMPTY_FRAGMENT =>
          ---SMFS_STREAM_EMPTY_FRAGMENT------------------------------------------
        when SMFS_ERROR =>
          ---SMFS_ERROR----------------------------------------------------------
          SMFS_State <= SMFS_RESET;                       
        when others =>
          SMFS_State <= SMFS_ERROR;                       
      end case;
    end if;
  end process state_machine;

  -- decoder interface
  decoder_intf: process (clk, reset) is
  begin  -- process decoder_intf
    if reset = '1' then                 -- asynchronous reset (active high)
    elsif clk'event and clk = '1' then  -- rising clock edge
      case SMFS_state is
        when SMFS_RESET | SMFS_INIT =>
          ---SMFS_RESET---SMFS_INIT----------------------------------------------
          stream_fragment <= '0';
          stream_start_pos_valid <= '0';          
        when SMFS_IDLE =>
          ---SMFS_IDLE-----------------------------------------------------------
          if stream_start_in = '1'  then
            --latch the start position
            stream_word_pos <= stream_pos_in;
            stream_start_pos_valid <= '1';
          end if;
        when SMFS_STREAM =>
          ---SMFS_STREAM---------------------------------------------------------
          case meta_word is
            when FM_BX_WORD =>
          end case;  
          --stream_fragment <= '1';          
        when others =>NULL;
      end case;
    end if;
  end process decoder_intf;

  
  -- map interface
  map_interface: process (clk, reset) is
  begin  -- process map_interface
    if reset = '1' then                 -- asynchronous reset (active high)
      
    elsif clk'event and clk = '1' then  -- rising clock edge
      datastream_trigger_done <= '0';
      case SMFS_state is
        when SMFS_STREAM =>
          ---SMFS_STREAM---------------------------------------------------------
          if eFIFO_meta_data_full = '0' and meta_word = FM_SIZE_LSB_WORD then            
            datastream_trigger_done <= '1';
          end if;
        when others => NULL;
      end case;
      
    end if;
  end process map_interface;

  -- event FIFO interface
  eFIFO_DAQ_stream_data_out    <= eFIFO_stream_data_out;
  eFIFO_DAQ_stream_data_out_dv <= eFIFO_stream_data_out_dv and datastream_trigger_DAQ_type;
  eFIFO_DAQ_meta_data_out      <= eFIFO_meta_data_out;
  eFIFO_DAQ_meta_data_out_dv   <= eFIFO_meta_data_out_dv and datastream_trigger_DAQ_type;

  eFIFO_LUMI_stream_data_out    <= eFIFO_stream_data_out;
  eFIFO_LUMI_stream_data_out_dv <= eFIFO_stream_data_out_dv and datastream_trigger_LUMI_type;
  eFIFO_LUMI_meta_data_out      <= eFIFO_meta_data_out;
  eFIFO_LUMI_meta_data_out_dv   <= eFIFO_meta_data_out_dv and datastream_trigger_LUMI_type;
  
  event_fifo_interface: process (clk, reset) is
  begin  -- process event_fifo_interface
    if reset = '0' then                 -- asynchronous reset (active low)
      
    elsif clk'event and clk = '1' then  -- rising clock edge
      eFIFO_meta_data_dv <= '0';
      efifo_words_reset  <= '0';
      stream_fragment    <= '0';

      
      case SMFS_state is
        when SMFW_GET_INFO =>
          --reset our word count
          efifo_words_reset <= '1';
        when SMFS_STREAM =>
          if ( (datastream_trigger_DAQ_type  and eFIFO_DAQ_meta_data_full) = '0' and
               (datastream_trigger_LUMI_type and eFIFO_LUMI_meta_data_full) = '0' )
               then
            --TODO We should adjust the eFIFO_DAQ_meta_data_full flag to be when we
            --can't fit a full control block into it. (5 writes
            case meta_word is
              when FM_BX_WORD =>
                eFIFO_meta_data_out    <= (others => '0');
                eFIFO_meta_data_out(8) <= '1';
                eFIFO_meta_data_out(datastream_trigger_ID'Range) <= datastream_trigger_ID;
                eFIFO_meta_data_dv <= '1';
                --move to the next work
                meta_word <= FM_START_BIT_WORD;
              when FM_START_BIT_WORD =>
                eFIFO_meta_data_out    <= (others => '0');
                eFIFO_meta_data_out(stream_word_pos'Range) <= stream_word_pos;
                eFIFO_meta_data_dv <= '1';
                --move to the next word
                meta_word <= FM_STOP_BIT_WORD;
              when FM_STOP_BIT_WORD =>
                --we wait here counting words written.
                if ( (datastream_trigger_DAQ_type  and eFIFO_DAQ_stream_data_full) = '0' and
                     (datastream_trigger_LUMI_type and eFIFO_LUMI_stream_data_full) = '0' )then
                  --TODO update the eFIFO_stream fifo to be full when there are
                  --only 1 or two works open.
                  stream_fragment <= '1';
                  --TODO Should we move the barrel shifter here? 
                  eFIFO_stream_data_out <= (others => '0');
                  eFIFO_stream_data_out(stream_data_in'Range) <= stream_data_in;
                  eFIFO_stream_data_out_dv <= stream_data_in_dv;                  
                end if;
                if stream_end_in = '1' then                  
                  --latch the stop position
                  eFIFO_meta_data_out    <= (others => '0');
                  eFIFO_meta_data_out(stream_word_pos'Range) <= stream_pos_in;
                  eFIFO_meta_data_dv <= '1';
                  --move to the next word
                  meta_word <= FM_SIZE_MSB_WORD;        
                end if;
              when FM_SIZE_MSB_WORD =>
                --latch the stop position
                eFIFO_meta_data_out    <= (others => '0');
                eFIFO_meta_data_out(7 downto 0) <= efifi_words_written(31 downto 16);
                eFIFO_meta_data_dv <= '1';
                --move to the next word
                meta_word <= FM_SIZE_LSB_WORD;
              when FM_SIZE_LSB_WORD =>
                --latch the stop position
                eFIFO_meta_data_out    <= (others => '0');
                eFIFO_meta_data_out(7 downto 0) <= efifi_words_written(15 downto 0);
                eFIFO_meta_data_dv <= '1';
                --move to the next word
                meta_word <= FM_BX_WORD;                        
            end case;
          end if;
          
        when others => NULL;
    end if;
  end process event_fifo_interface;

  efifo_words_written(1 downto 0) <= "00";
  efifo_word_write_counter_1: entity work.counter
    generic map (
      roll_over   => '0',
      DATAWIDTH   => 30)   -- we are couting 64 bit words, but we care about 16
                           -- bit words
    port map (
      clk         => clk,
      reset_async => '0',
      reset_sync  => efifo_words_reset,
      enable      => stream_fragment,
      event       => stream_data_in_dv,
      count       => efifo_words_written(31 downto 2),
      at_max      => open);

  
end architecture behavioral;
