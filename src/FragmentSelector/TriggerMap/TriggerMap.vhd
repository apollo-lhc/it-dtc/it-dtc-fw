library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

use work.types.all;

use work.RD53_Params_PKG.all;
use work.Trigger_PKG.all;
use work.TriggerMap_PKG.all;

entity TriggerMap is
  
  port (
    clk                               : in  std_logic;
    reset                             : in  std_logic;

    --triggers expected to show up eventually
    expected_trigger                  : in  Trigger10b_t;
    expected_trigger_dv               : in  std_logic;
                                      
    --datastream tag                  
    datastream_trigger_tag            : in  std_logic_vector(7 downto 0);
    datastream_trigger_tag_dv         : in  std_logic;
    datastream_trigger_done           : in  std_logic;

    datastream_trigger_valid          : out std_logic;
    datastream_trigger_not_found      : out std_logic;
    datastream_trigger_expect_no_data : out std_logic;
    datastream_trigger_ID             : out std_logic_vector(FE_BX_ID_SIZE-1+2 downto 0);
    datastream_trigger_DAQ_type       : out std_logic;
    datastream_trigger_LUMI_type      : out std_logic;

    --triggers to return to the trigger manager
    returned_tag                      : out std_logic_vector(5 downto 0);
    returned_tag_dv                   : out std_logic;

    --counters
    counter                     : out slv32_array_t(6 downto 0)
    );

end entity TriggerMap;

architecture behavioral of TriggerMap is

  -------------------------------------------------------------------------------
  -- tag data storage
  -------------------------------------------------------------------------------
  signal state_SMTM : SMTM_state_t;
  signal tag_data_ready : std_logic;
  
  -------------------------------------------------------------------------------
  -- Pipeline signals
  -------------------------------------------------------------------------------  
  signal current_tag       : Tag_t;--std_logic_vector(5 downto 0);
  signal current_trig_mask : std_logic_vector(3 downto 0);

  signal scheduled_trig_info     : tag_data_t;
  signal scheduled_trig_info_dv  : std_logic;
  signal scheduled_trig_info_ack : std_logic;

  signal tag_mask            : std_logic_vector(3 downto 0);
  signal current_trig_BX_int : integer range 0 to 3;
  signal current_trig_BX     : std_logic_vector(1 downto 0);
  
  signal datastream_trigger_valid_local : std_logic;

  signal mismatch_2bBX_ID : integer range 0 to 3;
  signal data_mismatch    : std_logic;
  signal start_streaming  : std_logic;
  
  -------------------------------------------------------------------------------
  --
  -------------------------------------------------------------------------------
  signal error_unexpected_datastream_trigger : std_logic;
  signal error_unexpected_datastream_tag     : std_logic;
  signal error_unexpected_datastream_tag_BX  : std_logic;
  signal counter_pulse                         : std_logic_vector(counter'RANGE) ;
begin  -- architecture behavioral

  -------------------------------------------------------------------------------
  -- State machine for tag data access (SMTM)
  -------------------------------------------------------------------------------
  master_state_machine: process (clk, reset) is
  begin  -- process 
    if reset = '1' then                 -- asynchronous reset (active high)
      state_SMTM <= SMTM_RESET;
    elsif clk'event and clk = '1' then  -- rising clock edge
      case state_SMTM is       
        when SMTM_RESET =>
          ---SMTM_RESET-----------------------------------------------------------
          state_SMTM <= SMTM_INIT;
        when SMTM_INIT  =>
          ---SMTM_INIT------------------------------------------------------------
          if tag_data_ready = '1' then
            state_SMTM <= SMTM_IDLE;
          else
            state_SMTM <= SMTM_INIT;
          end if;
        when SMTM_IDLE =>
          ---SMTM_IDLE------------------------------------------------------------
          if datastream_trigger_tag_dv = '1' then
            --process incomming trigger
            state_SMTM <= SMTM_PROCESSING;
          else
            state_SMTM <= SMTM_IDLE;
          end if;
        when SMTM_PROCESSING =>
--          if datastream_trigger_valid_local = '1' then
          if start_streaming = '1' then
            state_SMTM <= SMTM_STREAM;
          elsif data_mismatch = '1' then
            state_SMTM <= SMTM_DATA_MISMATCH;
          else
            state_SMTM <= SMTM_PROCESSING;
          end if;

          
        when SMTM_STREAM =>
          ---SMTM_PIPELINE_ACCESS------------------------------------------------
          if datastream_trigger_done = '1' then
            state_SMTM <= SMTM_IDLE;
          else
            state_SMTM <= SMTM_STREAM;
          end if;
        when SMTM_DATA_MISMATCH =>
          ---SMTM_DATA_MISMATCH--------------------------------------------------
          if scheduled_trig_info_ack = '1' then
            --We've moved out the tag_info that was
            --determined to be bad, now send the proper info
            --from current_tag*
            state_SMTM <= SMTM_PROCESSING;
          else
            statE_SMTM <= SMTM_DATA_MISMATCH;
          end if;
        when SMTM_ERROR =>
          ---SMTM_ERROR-----------------------------------------------------------
          state_SMTM <= SMTM_RESET;                
        when others => null;
      end case;
    end if;
  end process master_state_machine;   






  -------------------------------------------------------------------------------
  -- Tag data control
  -------------------------------------------------------------------------------  
  TagDataController_1: entity work.TagDataController
    port map (
      clk                         => clk,
      state_SMTM_i                => state_SMTM,
      ready                       => tag_data_ready,
      next_fragment_info_o        => scheduled_trig_info,
      next_fragment_info_dv_o     => scheduled_trig_info_dv,
      next_fragment_info_ack_i    => scheduled_trig_info_ack,
      expected_fragment_info_i.partial_BX_id => expected_trigger.partial_bx_ID,
      expected_fragment_info_i.tag           => expected_trigger.FE_trig.tag,
      expected_fragment_info_i.mask_DAQ      => expected_trigger.bx_mask_DAQ,
      expected_fragment_info_i.mask_LUMI     => expected_trigger.bx_mask_LUMI,
      expected_fragment_info_dv_i            => expected_trigger_dv);

  
  -------------------------------------------------------------------------------
  -- Pipeline controller
  -------------------------------------------------------------------------------
  datastream_trigger_valid <= datastream_trigger_valid_local;
  current_trig_BX_int <= to_integer(unsigned(current_trig_BX));
  pipeline_controller: process (clk, reset) is
  begin  -- process pipeline_controller
    if reset = '1' then                 -- asynchronous reset (active high)
      datastream_trigger_valid_local <= '0';
      scheduled_trig_info_ack                  <= '0';

      current_tag                    <= (others => '1');
      tag_mask                       <= x"F";
      returned_tag_dv                <= '0';
      returned_tag                   <= (others => '0');
      
      datastream_trigger_ID          <= (others => '0');
      datastream_trigger_DAQ_type    <= '0';
      datastream_trigger_LUMI_type   <= '0';

      start_streaming                <= '0';
      data_mismatch                  <= '0';
      
    elsif clk'event and clk = '1' then  -- rising clock edge
      datastream_trigger_valid_local <= '0';
      scheduled_trig_info_ack                  <= '0';
      returned_tag_dv                <= '0';
                                     
      start_streaming                <= '0';
      data_mismatch                  <= '0';
      
      case state_SMTM is
        when SMTM_INIT =>
          tag_mask <= x"F";

          
        when SMTM_IDLE =>
          ---SMTM_IDLE------------------------------------------------------------
          if datastream_trigger_tag_dv = '1' then
            --latch tag
            current_tag       <= tag_t(datastream_trigger_tag(7 downto 2));            
            --get trig number as an integer
            current_trig_BX <= datastream_trigger_tag(1 downto 0);
            --create a trig mask using the trig number (2 ** tag(1 downto 0))
            current_trig_mask <= (others => '0');
            current_trig_mask(to_integer(unsigned(datastream_trigger_tag(1 downto 0)))) <= '1';
          end if;

          
        when SMTM_PROCESSING =>
          -----SMTM_PROCESSING---------------------------------------------------

          datastream_trigger_ID             <= (others => '0');
          datastream_trigger_DAQ_type       <= '0';
          datastream_trigger_LUMI_type      <= '0';          

          if scheduled_trig_info_dv = '1' then
            if scheduled_trig_info.tag = Tag_t(current_tag) then
              -- This is an expected tag-----------------------------------------
              --we have data
              datastream_trigger_expect_no_data <= '0';
              
              if (scheduled_trig_info.mask_DAQ (current_trig_BX_int)  = '1' or
                  scheduled_trig_info.mask_LUMI(current_trig_BX_int)  = '1' ) then
                --This BX was expected
                datastream_trigger_not_found      <= '0';
              else
                --This BX was not expected
                datastream_trigger_not_found      <= '1';
              end if;
              
              datastream_trigger_ID        <= scheduled_trig_info.partial_bx_id & current_trig_BX;
              datastream_trigger_DAQ_type  <= scheduled_trig_info.mask_DAQ(current_trig_BX_int);
              datastream_trigger_LUMI_type <= scheduled_trig_info.mask_LUMI(current_trig_BX_int);

              --Update tag bx mask that this trigger has been received. 
              tag_mask(current_trig_BX_int)         <= '0';

              --start sending data
              start_streaming <= '1';
              datastream_trigger_valid_local <= '1';
            elsif to_integer(unsigned(current_tag)) > TAG_COUNT then
              -- This is another kind of trigger,--------------------------------
              -- label it as not found for now. 
              datastream_trigger_not_found      <= '1';
              datastream_trigger_expect_no_data <= '0';
              --start sending data to a bad place
              start_streaming <= '1';
              datastream_trigger_valid_local    <= '1';
            else
              --This is a real trigger, but out of order.------------------------
              if (scheduled_trig_info.tag < current_tag or
                  (scheduled_trig_info.tag = LAST_VALID_TAG and
                   current_tag = FIRST_VALID_TAG)) then                
                --create empty entries for this trigger_info tag
                -- clearing this tag is handled by SMTM_DATA_MISMATCH
                data_mismatch <= '1';
                mismatch_2bBX_ID   <= 3;
              else
                --Pass this data off as not found
                datastream_trigger_ID        <= scheduled_trig_info.partial_bx_id & current_trig_BX;
                datastream_trigger_DAQ_type  <= scheduled_trig_info.mask_DAQ(current_trig_BX_int);
                datastream_trigger_LUMI_type <= scheduled_trig_info.mask_LUMI(current_trig_BX_int);

                datastream_trigger_not_found      <= '1';
                datastream_trigger_expect_no_data <= '0';
                --start sending data to a bad place
                start_streaming <= '1';
                datastream_trigger_valid_local    <= '1';
              end if;
            end if;
          end if;


          
        when SMTM_STREAM =>          
          ---SMTM_STREAM---------------------------------------------------------
          if scheduled_trig_info_dv = '1' then
            --clear the old tag if we're done with it
            if (or_reduce(scheduled_trig_info.mask_DAQ  and tag_mask) = '0' and
                or_reduce(scheduled_trig_info.mask_LUMI and tag_mask) = '0' ) then
              -- We have sent ever trigger for this tag
              -- pull it out of the FIFO
              scheduled_trig_info_ack <= '1';
              --return the tag
              returned_tag <= std_logic_vector(scheduled_trig_info.tag);
              returned_tag_dv <= '1';
              --reset our read tag mask
              tag_mask <= x"F";
            end if;
          else
            --wait until datastream_trigger_done moves our state
          end if;

          
        when SMTM_DATA_MISMATCH =>
          ---SMTM_DATA_MISMATCH--------------------------------------------------
          if datastream_trigger_done = '1' then
            --send out the remaining triggers for this tag
            if scheduled_trig_info_dv = '1' then            
              if (or_reduce(scheduled_trig_info.mask_DAQ  and tag_mask) = '0' and
                  or_reduce(scheduled_trig_info.mask_LUMI and tag_mask) = '0' ) then
                -- We have sent ever trigger for this tag
                -- pull it out of the FIFO
                scheduled_trig_info_ack <= '1';
                --return the tag
                returned_tag <= std_logic_vector(scheduled_trig_info.tag);
                returned_tag_dv <= '1';
                --reset our read tag mask
                tag_mask <= x"F";
              elsif (scheduled_trig_info.mask_DAQ(mismatch_2bBX_ID)  = '1' or
                     scheduled_trig_info.mask_LUMI(mismatch_2bBX_ID) = '1' ) then
                -- Send an empty structure for this tag+bx 

                datastream_trigger_ID             <= scheduled_trig_info.partial_bx_id & std_logic_vector(to_unsigned(mismatch_2bBX_ID,2));
                datastream_trigger_DAQ_type       <= scheduled_trig_info.mask_DAQ (mismatch_2bBX_ID);
                datastream_trigger_LUMI_type      <= scheduled_trig_info.mask_LUMI(mismatch_2bBX_ID);
                

                datastream_trigger_not_found      <= '0';
                datastream_trigger_expect_no_data <= '1';

                
                tag_mask(mismatch_2bBX_ID)     <= '0';
                datastream_trigger_valid_local    <= '1';
              end if;
              if mismatch_2bBX_ID /= 0 then
                mismatch_2bBX_ID <= mismatch_2bBX_ID -1;
              end if;

            end if;
          end if;
        when others => NULL;
      end case;
    end if;
  end process pipeline_controller;



  -------------------------------------------------------------------------------
  -- Error monitoring
  -------------------------------------------------------------------------------
  monitoring: process (clk, reset) is
  begin  -- process 
    if reset = '1' then                 -- asynchronous reset (active high)
      error_unexpected_datastream_trigger <= '0';
      error_unexpected_datastream_tag     <= '0';
      error_unexpected_datastream_tag_BX  <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      error_unexpected_datastream_trigger <= '0';
      error_unexpected_datastream_tag     <= '0';
      error_unexpected_datastream_tag_BX  <= '0';
      case state_SMTM is       
        when SMTM_PROCESSING =>
          if datastream_trigger_tag_dv = '1' then
            error_unexpected_datastream_trigger <= '1';
          end if;
          if scheduled_trig_info_dv = '1' then
            if scheduled_trig_info.tag /= Tag_t(current_tag) then
              error_unexpected_datastream_tag <= '1';
            end if;

            if (scheduled_trig_info.mask_DAQ(current_trig_BX_int) = '0' and
                scheduled_trig_info.mask_LUMI(current_trig_BX_int) = '0' ) then
              error_unexpected_datastream_tag_BX <= '1';
            end if;
              
          end if;

        when SMTM_STREAM =>
          if datastream_trigger_tag_dv = '1' then
            error_unexpected_datastream_trigger <= '1';
          end if;

          
        when others => NULL;
      end case;
    end if;
  end process monitoring;

  counter_pulse(0) <= error_unexpected_datastream_trigger;
  counter_pulse(1) <= error_unexpected_datastream_tag;
  counter_pulse(2) <= error_unexpected_datastream_tag_BX;
  counter_pulse(3) <= data_mismatch;
  counter_pulse(4) <= '1' when datastream_trigger_valid_local = '1' and state_SMTM = SMTM_DATA_MISMATCH else '0';
  counter_pulse(5) <= datastream_trigger_valid_local;
  counter_pulse(6) <= datastream_trigger_tag_dv ;

  counters: for iCounter in counter_pulse'range generate
    counter_1: entity work.counter
      generic map (
        roll_over   => '0')
      port map (
        clk         => clk,
        reset_async => reset,
        reset_sync  => '0',
        enable      => '1',
        event       => counter_pulse(iCounter),
        count       => counter(iCounter),
        at_max      => open);
  end generate counters;

    
  
end architecture behavioral;
