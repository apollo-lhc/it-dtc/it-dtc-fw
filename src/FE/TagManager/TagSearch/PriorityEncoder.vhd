library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

use IEEE.math_real."ceil";
use IEEE.math_real."log2";

entity PriorityEncoder is
  generic (
    INPUT_COUNT : natural);
  port (
    input         : in  std_logic_vector(INPUT_COUNT-1 downto 0);
    index         : out std_logic_vector(ceil(log2(real(INPUT_COUNT))) - 1 downto 0);
    valid         : out std_logic
    );
end entity PriorityEncoder;

architecture behavioral of PriorityEncoder is
  constant ROUNDED_COUNT_WIDTH : natural := ceil(log4(real(INPUT_COUNT)))
  constant ROUNDED_COUNT : natural := 4**(ROUNDED_COUNT_WIDTH);
  signal rounded_input : std_logic_vector(ROUNDED_COUNT-1 downto 0);
  signal index_lsbs_map : std_logic_vector(ROUNDED_COUNT/2 -1 downto 0);
  signal valid_lsbs : std_logic_vector(ROUNDED_COUNT/4 -1 downto 0);
  signal index_msbs_map : std_logic_vector();
begin
  rounded_input(INPUT_COUNT -1  downto 0) <= input;
  rounded_input(ROUNDED_COUNT downto INPUT_COUNT);

  priority_encoder_4bit_grouping: for iGroup in 0 to ceil(log4(real(INPUT_COUNT))) generate
    PE_4bitLSB_inst: PriorityEncoder_4bit_LSB
      port map (
        input => input((4*iGroup) + 4 - 1 downto (4*iGroup)),
        index => index_lsbs_map(iGroup*2 + 2 - 1 downto iGroup*2),
        valid => valid_lsbs(iGroup)
        );
    
  PE: process (valid_lsbs) is
  begin  -- process PE
    for iBit in ROUNDED_COUNT/4 -1 downto 0 loop
      valid <= '0';
      if valid_lsbs(iBit) = '1' then
        index(ceil(log2(real(INPUT_COUNT)))-1 downto 2) <= std_logic_vector(to_unsigned(iBit,index_msbs_map'length));
        index(1 downto 0) <= index_lsbs_map( (2*iBit) + 2 -1 downto (2*iBit));
        valid <= '1';
        exit
      end if;
    end loop;  -- iBit
  end process PE;
  
  end generate priority_encoder_4bit_grouping;
end architecture behavioral;
