library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

use work.RD53_Params_PKG.all;
use work.Trigger_PKG.all;

entity TagManager is
  generic (
    DOWNSTREAM_COUNT : integer := 1);
  port (
    --TCDS interface
    TCDS_clk                 : in  std_logic;
    TCDS_reset               : in  std_logic;
    TTC_trigger_ID           : in  std_logic_vector(BX_ID_SIZE-1 downto 0);
    TTC_trigger_type         : in  std_logic_vector(1 downto 0);
    TTC_trigger_dv           : in  std_logic;
    TTS_out_of_tags          : out std_logic;
    TTS_low_on_tags          : out std_logic;

    -- Front end
    FE_trigger_out           : out FE_Trigger10b_t;
    FE_trigger_out_dv        : out std_logic;

    --Downstream DAQ stuff
    Pipeline_clk             : in  std_logic;

    --these are on the Pipeline clock (so fix this in the rest of the module)
    downstream_reset         : out std_logic;
    downstream_ready         : in  std_logic_vector(DOWNSTREAM_COUNT-1 downto 0);
    
    returned_tag             : in  Tag_t;
    returned_tag_dv          : in  std_logic;

    fragment_sel_trigger_out              : out Trigger10b_t;
    fragment_sel_trigger_out_dv           : out std_logic;

    eventbuilder_trigger_out : out EventBuilder_trigger_t;
    eventbuilder_trigger_dv  : out std_logic
    
    );
end entity TagManager;

architecture structure of TagManager is

  type TG_State_t is (TGS_RESET,
                      TGS_INIT,
                      TGS_RUNNING,
                      TGS_PAUSE,
                      TGS_STOP,
                      TGS_OUT_OF_TAGS,
                      TGS_ERROR
                      );
  signal TG_state         : TG_State_t := TGS_RESET;

  constant RESET_COUNT    : integer := 10;
  signal   reset_counter  : integer range 0 to RESET_COUNT;

  signal trigger_mask     : std_logic_vector(3 downto 0);
  signal trig_mask_DAQ    : std_logic_vector(3 downto 0);
  signal trig_mask_LUMI   : std_logic_vector(3 downto 0);
  signal mask_has_trigger : std_logic;
  signal take_tag_from_FIFO : std_logic;
  
  signal Tag_FIFO_reset   : std_logic;
  signal Tag_FIFO_ready   : std_logic;
  signal free_tag         : Tag_t;
  signal free_tag_dv      : std_logic;
begin  -- architecture structure


  reset_timer: process (TCDS_clk, TCDS_reset) is
  begin  -- process reset_timer
    if TCDS_reset = '1' then            -- asynchronous reset (active high)
      reset_counter <= RESET_COUNT;
    elsif TCDS_clk'event and TCDS_clk = '1' then  -- rising clock edge
      if TG_State = TGS_RESET then
        if reset_counter /= 0 then
          reset_counter <= reset_counter - 1;        
        end if;
      else
        --get ready for another transition to TGS_RESET
        reset_counter <= RESET_COUNT;
      end if;
    end if;
  end process reset_timer;
  
  StateMachine: process (TCDS_clk, TCDS_reset) is
  begin  -- process StateMachine
    if TCDS_reset = '1' then            -- asynchronous reset (active high)
      TG_State <= TGS_RESET;
      Tag_FIFO_reset <= '1'; --Hold in reset
    elsif TCDS_clk'event and TCDS_clk = '1' then  -- rising clock edge
--      if Ctrl.RESET_SYSTEM = '1' then
--        TG_State <= TGS_RESET;
--      else         
      case TG_State is
        when TGS_RESET =>
          ---TGS_RESET---------------------------------------------------------
          Tag_FIFO_reset <= '1'; --Hold in reset
          if reset_counter = 0 then
            --reset timer is over, start init
            TG_State <= TGS_INIT;          
            Tag_FIFO_reset <= '0';
          end if;
        when TGS_INIT =>
          ---TGS_INIT----------------------------------------------------------
          if Tag_FIFO_Ready = '1' and and_reduce(downstream_ready) = '1' then
            --Local TAG FIFO and everything downstream of us is ready for a run
            TG_State <= TGS_RUNNING;
          end if;
        when TGS_RUNNING =>
          ---TGS_RUNNING-------------------------------------------------------
          TG_State <= TGS_RUNNING;
          if free_tag_dv = '0' and TTC_Trigger_ID(1 downto 0) = "00" then
            TG_State <= TGS_OUT_OF_TAGS;
          end if;
        when TGS_OUT_OF_TAGS =>
          ---TGS_OUT_OF_TAGS---------------------------------------------------
          Tg_State <= TGS_ERROR;
        when TGS_ERROR =>
          ---TGS_ERROR---------------------------------------------------------
          TG_State <= TGS_RESET;
        when others =>
          ---OTHERS------------------------------------------------------------
          TG_State <= TGS_ERROR;
      end case;
--      end if;
    end if;
  end process StateMachine;
  
  --FIFO with filler
  Tag_FIFO: entity work.TagFIFO    
    port map(
      clk          => TCDS_clk,
      reset        => Tag_FIFO_reset,
      ready        => Tag_FIFO_Ready,
      free_tag     => free_tag,
      free_tag_dv  => free_tag_dv,
      free_tag_ack => take_tag_from_FIFO,
      used_tag     => returned_tag,
      used_tag_dv  => returned_tag_dv);  
  -------------------------------------------------------------------------------
  --trigger grouper
  -------------------------------------------------------------------------------

  -- FE trigger out (async)
  take_tag_from_FIFO         <= '1' when (TTC_trigger_ID(1 downto 0) = "00" and
                                        TG_state = TGS_RUNNING and
                                        mask_has_trigger = '1' and
                                        free_tag_dv = '1' and
                                        TTC_trigger_dv = '1')
                              else '0';
  FE_trigger_out_dv          <= take_tag_from_FIFO;
  FE_trigger_out.tag         <= free_tag;
  FE_trigger_out.tag_BX_mask <= Tag_BX_mask_t(trigger_mask);

  

  
  trigger_grouper: process (TCDS_clk, TCDS_reset) is
  begin  -- process trigger_grouper
    if TCDS_reset = '1' then                 -- asynchronous reset (active high)
      mask_has_trigger <= '0';
      trigger_mask     <= (others => '0');
    elsif TCDS_clk'event and TCDS_clk = '1' then  -- rising clock edge
      case TG_state is
        when TGS_RUNNING =>
          ---TGS_RUNNING---------------------------------------------------------

          if TTC_Trigger_dv = '1' then
            --Set next FE trigger mask
            mask_has_trigger <= mask_has_trigger or or_reduce(TTC_trigger_type);
            case TTC_Trigger_ID(1 downto 0) is
              when "00" =>
                trigger_mask <= "000" & ( or_reduce(TTC_trigger_type));
                mask_has_trigger <=  ( or_reduce(TTC_trigger_type));
              when "01" =>
                trigger_mask(1) <=  ( or_reduce(TTC_trigger_type));
              when "10" =>
                trigger_mask(2) <=  ( or_reduce(TTC_trigger_type));
              when "11" =>
                trigger_mask(3) <=  ( or_reduce(TTC_trigger_type));
              when others => NULL;
            end case;

            --Send the current fragment trigger
            case TTC_Trigger_ID(1 downto 0) is
              when "00" =>
                --fragment trigger (trig_ID in "11" state)
                fragment_sel_trigger_out.FE_trig.tag         <= free_tag;
                fragment_sel_trigger_out.FE_trig.tag_BX_mask <= trigger_mask;
                fragment_sel_trigger_out.BX_mask_DAQ         <= trig_mask_DAQ;
                fragment_sel_trigger_out.BX_mask_LUMI        <= trig_mask_LUMI;
                fragment_sel_trigger_out_dv <= take_tag_from_FIFO;
                --setup for next period 4
                trig_mask_DAQ  <= "000" & TTC_Trigger_type(0);
                trig_mask_LUMI <= "000" & TTC_Trigger_type(1);
              when "01" =>
                --setup for next period 4
                trig_mask_DAQ(1)  <= TTC_Trigger_type(0);
                trig_mask_LUMI(1) <= TTC_Trigger_type(1);
              when "10" =>
                --setup for next period 4
                trig_mask_DAQ(2)  <= TTC_Trigger_type(0);
                trig_mask_LUMI(2) <= TTC_Trigger_type(1);
              when "11" =>
                --fragment trigger (used in next state "00")
                fragment_sel_trigger_out.partial_BX_id <= TTC_Trigger_ID(FE_BX_ID_SIZE+2-1 downto 2);
                --setup for next period 4
                trig_mask_DAQ(3)  <= TTC_Trigger_type(0);
                trig_mask_LUMI(3) <= TTC_Trigger_type(1);
              when others => null;                             
            end case;
          end if;
        when others => null;
      end case;
    end if;
  end process trigger_grouper;

end architecture structure;
