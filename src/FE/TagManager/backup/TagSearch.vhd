library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity TagSearch is
  
  generic (
    TAG_COUNT : integer := 64);

  port (
    tag_free_map   : in  std_logic_vector(TAG_COUNT-1 downto 0);
    free_tag       : out Tag_t;
    free_tag_valid : out std_logic);

end entity TagSearch;

architecture behavioral of TagSearch is

  signal tag_group_select     : std_logic_vector(2*(TAG_COUNT/4) -1 downto 0);
--groups of two bits
  signal tag_group_has_valid  : std_logic_vector(TAG_COUNT/4 -1 downto 0)
  
begin  -- architecture behavioral

  --First layer merges 4 channels to 1, 
  TagSearch_layer1_1: entity work.TagSearch_layer1
    generic map (
      TAG_COUNT => TAG_COUNT)
    port map (
      tag_free_map        => tag_free_map,
      tag_group_select    => tag_group_select,
      tag_group_has_valid => tag_group_has_valid);

  --layer 2 pass down layer 1 , compare and get bit 2  
  

  
end architecture behavioral;
