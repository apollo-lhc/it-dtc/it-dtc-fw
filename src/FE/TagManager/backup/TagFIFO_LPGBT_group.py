#!/usr/bin/env python 

TAG_COUNT = 64
CHIP_COUNT = 32

class TagFifo:
    def Reset(self):
        self.freeTags = [(2**CHIP_COUNT)-1 for i in range(TAG_COUNT)]
    def __init__(self):
        self.Reset()
    #debug
    def PrintTags(self):
        for iTag in range(TAG_COUNT):
            print("%02X: %08X\n" % (iTag,self.freeTags[iTag]))

    def GetTag():
        ####################### Stage 1 #########################################
        #Build free tag boolean
        tagIsFree = [1 for i in range(TAG_COUNT)]
        for iTag in range(TAG_COUNT):
            for iBit in range(CHIP_COUNT):
                tagIsFree[iTag] = tagIsFree[iTag] and (self.freeTags[iTag] & (1 << iBit))
        ####################### Stage 2 #########################################
        #tagIsFree now lists if the tag is free or not
        tagLSN = [0 for i in range(TAG_COUNT/4)]
        tagLSNFree = [0 for i in range(TAG_COUNT/4)]
        for iGroup in range(TAG_COUNT/4):
            #loop over grouping of four tags (2-lsb)
            for iTag in range(4):
                if tagIsFree[iGroup*4 + iTag]:
                    #take the lowest free tag
                    tagLSNFree = 1
                    tagLSN=iTag
                    break
        ####################### Stage 3 #########################################
        for iGroup in range(TAG_COUNT/8):
            if tagLSNFree
        

if __name__ == "__main__":
    TF= TagFifo()

    TF.PrintTags()
    
