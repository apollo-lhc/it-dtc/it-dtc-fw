library ieee;
use ieee.std_logic_1164.all;

package LPGBT_IO_PKG is

  type LPGBT_in_t is record
    user_data : std_logic_vector(31 downto  0);
    ec_data   : std_logic_vector( 1 downto  0);
    ic_data   : std_logic_vector( 1 downto  0);
  end record LPGBT_in_t;

  type LPGBT_in_array_t is array (integer range<>) of LPGBT_in_t;
  
  type LPGBT_out_t is record
    data_valid : std_logic;
    user_data  : std_logic_vector(229 downto 0);
    ec_data    : std_logic_vector( 1 downto  0);
    ic_data    : std_logic_vector( 1 downto  0);
  end record LPGBT_out_t;

  type LPGBT_out_array_t is array (integer range<>) of LPGBT_out_t;

end package LPGBT_IO_PKG;
