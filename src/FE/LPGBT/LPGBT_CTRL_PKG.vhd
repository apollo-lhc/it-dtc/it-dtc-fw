library ieee;
use ieee.std_logic_1164.all;

package LPGBT_Ctrl_PKG is

  --Downlink
  type LPGBT_Downlink_Ctrl_t is record
    bypass_interleaver : std_logic;
    bypass_encoder     : std_logic;
    bypass_scrambler   : std_logic;
  end record LPGBT_Downlink_Ctrl_t;

  
  constant LPGBT_DOWNLINK_CTRL_DEFAULT : LPGBT_Downlink_Ctrl_t := (bypass_interleaver => '0',
                                                                   bypass_encoder     => '0',
                                                                   bypass_scrambler   => '0');

  type LPGBT_Downlink_Mon_t is record
    ready   : std_logic;
    reset_n : std_logic;
  end record LPGBT_Downlink_Mon_t;

  
  --Uplink
  type LPGBT_Uplink_Ctrl_t is record
    bypass_interleaver : std_logic;
    bypass_FEC_encoder : std_logic;
    bypass_scrambler   : std_logic;
  end record LPGBT_Uplink_Ctrl_t;

  
  constant LPGBT_UPLINK_CTRL_DEFAULT : LPGBT_Uplink_Ctrl_t := (bypass_interleaver => '0',
                                                               bypass_FEC_encoder => '0',
                                                               bypass_scrambler   => '0');

  type LPGBT_Uplink_Mon_t is record
    corrected_data   : std_logic_vector(229 downto 0); 
    corrected_ic     : std_logic_vector(1 downto 0);   
    corrected_ec     : std_logic_vector(1 downto 0);   
    frame_align_even : std_logic;
    ready            : std_logic;
    reset_n          : std_logic;
  end record LPGBT_Uplink_Mon_t;

  
  --top
  type LPGBT_Ctrl_t is record
    downlink : LPGBT_Downlink_Ctrl_t;
    uplink   : LPGBT_Uplink_Ctrl_t;
  end record LPGBT_Ctrl_t;

  type LPGBT_Ctrl_array_t is array (integer range<>) of LPGBT_Ctrl_t;

  constant LPGBT_CTRL_DEFAULT : LPGBT_Ctrl_t := (downlink => LPGBT_DOWNLINK_CTRL_DEFAULT,
                                                 uplink   => LPGBT_UPLINK_CTRL_DEFAULT);
  
  
  type LPGBT_Mon_t is record
    downlink : LPGBT_Downlink_Mon_t; 
    uplink   : LPGBT_Uplink_Mon_t;       
  end record LPGBT_Mon_t;

  type LPGBT_Mon_array_t is array (integer range<>) of LPGBT_Mon_t;

  
  
end package LPGBT_CTRL_PKG;
