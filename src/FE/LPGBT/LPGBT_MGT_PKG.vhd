library ieee;
use ieee.std_logic_1164.all;

package LPGBT_MGT_PKG is

  type LPGBT_MGT_in_upstream_t is record
    ready     : std_logic;
    data      : std_logic_vector(31 downto 0);
  end record LPGBT_MGT_in_upstream_t;    

  type LPGBT_MGT_in_downstream_t is record
    ready     : std_logic;
  end record LPGBT_MGT_in_downstream_t;
  
  type LPGBT_MGT_in_upstream_array_t is array (integer range<>) of LPGBT_MGT_in_upstream_t;
  type LPGBT_MGT_in_downstream_array_t is array (integer range<>) of LPGBT_MGT_in_downstream_t;

  type LPGBT_MGT_out_downstream_t is record
    data : std_logic_vector(31 downto  0);
  end record LPGBT_MGT_out_downstream_t;

  type LPGBT_MGT_out_downstream_array_t is array (integer range<>) of LPGBT_MGT_out_downstream_t;

  type LPGBT_MGT_out_upstream_t is record
    bitslip : std_logic;
  end record LPGBT_MGT_out_upstream_t;

  type LPGBT_MGT_out_upstream_array_t is array (integer range<>) of LPGBT_MGT_out_upstream_t;


end package LPGBT_MGT_PKG;
