library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

use work.axiRegPkg.all;
use work.axiRegPkg_d64.all;
use work.types.all;
--use work.IO_Ctrl.all;
--use work.C2C_INTF_CTRL.all;
use work.AXISlaveAddrPkg.all;

--HAL packages for MGTs
use work.HAL_TOP_IO_PKG.all;
use work.HAL_PKG.all;
use work.F1_LPGBT_PKG.all;

--Packages used in this file for LPGBT stuff
use work.LPGBT_GENERIC_PKG.all;              
use work.LPGBT_CTRL_PKG.all;
use work.LPGBT_IO_PKG.all;
use work.LPGBT_MGT_PKG.all;

--actual LPGBT package
use work.lpgbtfpga_package.all;

--PL registers
use work.ITDTC_IO_Ctrl.all;


Library UNISIM;
use UNISIM.vcomponents.all;

entity itdtc_top is
  generic (
    FREERUN_FREQ         : integer := 50000000;
    AXI_CONNECTION_COUNT : integer;
    HAL_MEMORY_RANGE     : unsigned;
    IO_MEM_RANGE         : unsigned;
    TCDS_MEM_RANGE       : unsigned    
    );
  port (
    -- clocks
    clk_freerun        : in  std_logic;
    clk_freerun_locked : in  std_logic;
    
    --TCDS
    TCDS_BP_clk_p      : in  std_logic;
    TCDS_BP_clk_n      : in  std_logic;
    clk_40Mhz_out_p    : out std_logic;
    clk_40Mhz_out_n    : out std_logic;
    clk320             : out std_logic;
    clk_320en40        : out std_logic;
    clk_locked_320     : out std_logic;
    
    --HAL              
    HAL_refclks        : in  HAL_refclks_t;
    HAL_serdes_input   : in  HAL_serdes_input_t;
    HAL_serdes_output  : out HAL_serdes_output_t;

    --AXI    
    axi_clk            : in  std_logic;
    axi_rst_n          : in  std_logic;
    AXI_ReadMOSI       : in  AXIReadMOSI_array_t (0 to AXI_CONNECTION_COUNT-1);
    AXI_ReadMISO       : out AXIReadMISO_array_t (0 to AXI_CONNECTION_COUNT-1);
    AXI_WriteMOSI      : in  AXIWriteMOSI_array_t(0 to AXI_CONNECTION_COUNT-1);
    AXI_WriteMISO      : out AXIWriteMISO_array_t(0 to AXI_CONNECTION_COUNT-1)
    
    );
end entity itdtc_top;

architecture structure of itdtc_top is

  constant LPGBT_IP_CONFIG : LPGBT_generic_t := (
    up    => (DATARATE                    => DATARATE_10G24,
              FEC                         => FEC12 ,
              MULTICYCLE_DELAY            => 3,
              CLOCK_RATIO                 => 8,
              WORD_WIDTH                  => 32,
              ALLOWED_FALSE_HEADER        => 5,
              ALLOWED_FALSE_HEADER_OVER_N => 64,
              REQUIRED_TRUE_HEADER        => 30,
              BITSLIP_MINDLY              => 1,
              BITSLIP_WAIT_DELAY          => 40
              ),      
    down   => (MULTICYCLE_DELAY => 3,
               CLOCK_RATIO      => 8,
               WIDTH            => 32           
               )
    );
  constant LPGBT_IP_CONFIGs : lpgbt_generic_array_t(0 to F1_LPGBT_CHAN_COUNT-1) := (others => LPGBT_IP_CONFIG);

  
  -- MGT data
  signal LPGBT_userdata_input  : F1_LPGBT_userdata_input_array_t(0 to F1_LPGBT_CHAN_COUNT-1);
  signal LPGBT_userdata_output : F1_LPGBT_userdata_output_array_t(0 to F1_LPGBT_CHAN_COUNT-1);
--  signal LPGBT_clocks_output   : f

  --fake TCDS data
  signal clk_320 : std_logic;
  signal clken_40 : std_logic;
  signal locked_320 : std_logic;
  
  -- LPGBT data
  signal LPGBT_MGT_up_in     : LPGBT_MGT_in_upstream_array_t(0 to F1_LPGBT_CHAN_COUNT-1);
  signal LPGBT_MGT_up_out    : LPGBT_MGT_out_upstream_array_t(0 to F1_LPGBT_CHAN_COUNT-1);   
  signal LPGBT_MGT_down_in   : LPGBT_MGT_in_downstream_array_t(0 to F1_LPGBT_CHAN_COUNT-1);
  signal LPGBT_MGT_down_out  : LPGBT_MGT_out_downstream_array_t(0 to F1_LPGBT_CHAN_COUNT-1);

  signal LPGBT_Ctrl : LPGBT_Ctrl_array_t(0 to F1_LPGBT_CHAN_COUNT-1) := (others => LPGBT_CTRL_DEFAULT);
  signal LPGBT_Mon  : LPGBT_Mon_array_t(0 to F1_LPGBT_CHAN_COUNT-1) ;

  signal LPGBT_in   : LPGBT_in_array_t(0 to F1_LPGBT_CHAN_COUNT-1); 
  signal LPGBT_out  : LPGBT_out_array_t(0 to F1_LPGBT_CHAN_COUNT-1);

  signal ITDTC_IO_CTRL : ITDTC_IO_CTRL_t;
  signal ITDTC_IO_MON  : ITDTC_IO_MON_t;
  
begin        
  FAKE_TCDS_2: entity work.FAKE_TCDS
    generic map (
      ALLOCATED_MEMORY_RANGE => to_integer(TCDS_MEM_RANGE),
      FREERUN_FREQ => FREERUN_FREQ)
    port map (
      freerun_clk     => clk_freerun,
      freerun_rst     => not clk_freerun_locked,
      TCDS_BP_clk_p   => TCDS_BP_clk_p,
      TCDS_BP_clk_n   => TCDS_BP_clk_n,
      locked_320Mhz   => locked_320,
      clk_320Mhz      => clk_320,
      clk_320en40     => clken_40,
      clk_40Mhz_out_p => clk_40Mhz_out_p,
      clk_40Mhz_out_n => clk_40Mhz_out_n,
      clk_axi         => axi_clk,
      reset_axi_n     => axi_rst_n,
      slave_readMOSI  => axi_readMOSI(2),
      slave_readMISO  => axi_readMISO(2),
      slave_writeMOSI => axi_writeMOSI(2),
      slave_writeMISO => axi_writeMISO(2)
      );
  clk320 <= clk_320;
  clk_320en40 <= clken_40;
  clk_locked_320 <= locked_320;

  HAL_1: entity work.HAL
    generic map (
      F1_LPGBT_MEMORY_RANGE => to_integer(HAL_MEMORY_RANGE)
      )
    port map (
      clk_axi                      => axi_clk,
      reset_axi_n                  => axi_rst_n,
      readMOSI                     => AXI_readMOSI(0 to 0),
      readMISO                     => AXI_readMISO(0 to 0),
      writeMOSI                    => AXI_writeMOSI(0 to 0),
      writeMISO                    => AXI_writeMISO(0 to 0),
      HAL_refclks                  => HAL_refclks,
      HAL_serdes_input             => HAL_serdes_input, 
      HAL_serdes_output            => HAL_serdes_output,
      F1_LPGBT_userdata_input      => LPGBT_userdata_input,
      F1_LPGBT_userdata_output     => LPGBT_userdata_output,
      F1_LPGBT_clocks_output       => open);


  

  
  lpgbt_convert: for iLPGBT in 0 to F1_LPGBT_CHAN_COUNT-1 generate

    --From LPGBTFPGA to MGT

    -----------------------------------------------------------------------------
    --From MGT to LPGBTFPGA
    -----------------------------------------------------------------------------
    --MGT tx Resets done
    LPGBT_MGT_down_in(iLPGBT).ready <= LPGBT_userdata_output(iLPGBT).txresetdone(0);
    --MGT rx Resets done
    LPGBT_MGT_up_in(iLPGBT).ready   <= LPGBT_userdata_output(iLPGBT).rxresetdone(0);
    --MGT rx data to LPGBTFPGA
    LPGBT_MGT_up_in(iLPGBT).data    <= LPGBT_userdata_output(iLPGBT).GTWIZ_USERDATA_RX;
    
    -----------------------------------------------------------------------------
    --From LPGBTFPGA to MGT
    -----------------------------------------------------------------------------
    --LPGBTFPGA to MGT tx data
    LPGBT_userdata_input(iLPGBT).GTWIZ_USERDATA_TX <= LPGBT_MGT_down_out(iLPGBT).data;
    LPGBT_userdata_input(iLPGBT).rxslide(0)        <= LPGBT_MGT_up_out(iLPGBT).bitslip;


    -----------------------------------------------------------------------------
    --From LPGBTFPGA to user
    -----------------------------------------------------------------------------
    -- user data
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).UPLINK.user_data_0(31 downto 0)  <= LPGBT_out(iLPGBT).user_data( 31 downto   0);
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).UPLINK.user_data_1(31 downto 0)  <= LPGBT_out(iLPGBT).user_data( 63 downto  32);
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).UPLINK.user_data_2(31 downto 0)  <= LPGBT_out(iLPGBT).user_data( 95 downto  64);
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).UPLINK.user_data_3(31 downto 0)  <= LPGBT_out(iLPGBT).user_data(127 downto  96);
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).UPLINK.user_data_4(31 downto 0)  <= LPGBT_out(iLPGBT).user_data(159 downto 128);
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).UPLINK.user_data_5(31 downto 0)  <= LPGBT_out(iLPGBT).user_data(191 downto 160);
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).UPLINK.user_data_6(31 downto 0)  <= LPGBT_out(iLPGBT).user_data(223 downto 192);
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).UPLINK.user_data_7( 5 downto 0)  <= LPGBT_out(iLPGBT).user_data(229 downto 224);
    -- EC data
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).UPLINK.ec         <= LPGBT_out(iLPGBT).ec_data;
    -- IC data
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).UPLINK.ic         <= LPGBT_out(iLPGBT).ic_data;
    -- data valid
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).UPLINK.data_valid <= LPGBT_out(iLPGBT).data_valid;
    --downlink ready
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).DOWNLINK.ready      <= LPGBT_MON(iLPGBT).downlink.ready;
    -- downlink reset_n
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).DOWNLINK.reset_n    <= LPGBT_MON(iLPGBT).downlink.reset_n;
    -- uplink ready
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).UPLINK.ready      <= LPGBT_MON(iLPGBT).uplink.ready;
    -- uplink reset_n
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).UPLINK.reset_n    <= LPGBT_MON(iLPGBT).uplink.reset_n;

   
    -----------------------------------------------------------------------------
    --From user to LPGBTFPGA
    -----------------------------------------------------------------------------
    --sample user data
    LPGBT_in(iLPGBT).user_data <= ITDTC_IO_Ctrl.LPGBT(iLPGBT+1).DOWNLINK.user_data;
    --sample ec data
    LPGBT_in(iLPGBT).ec_data   <= ITDTC_IO_Ctrl.LPGBT(iLPGBT+1).DOWNLINK.ec;
    --sample ic data
    LPGBT_in(iLPGBT).ic_data   <= ITDTC_IO_Ctrl.LPGBT(iLPGBT+1).DOWNLINK.ic;
    --LPGBT uplink configuration
    LPGBT_CTRL(iLPGBT).uplink.bypass_interleaver <= ITDTC_IO_Ctrl.LPGBT(iLPGBT+1).UPLINK.BYPASS.INTERLEAVER; 
    LPGBT_CTRL(iLPGBT).uplink.bypass_fec_encoder <= ITDTC_IO_Ctrl.LPGBT(iLPGBT+1).UPLINK.BYPASS.FEC_ENCODER; 
    LPGBT_CTRL(iLPGBT).uplink.bypass_scrambler   <= ITDTC_IO_Ctrl.LPGBT(iLPGBT+1).UPLINK.BYPASS.SCRAMBLER; 
    --LPGBT downlink configuration
    LPGBT_Ctrl(iLPGBT).downlink.bypass_interleaver <= ITDTC_IO_Ctrl.LPGBT(iLPGBT+1).DOWNLINK.BYPASS.INTERLEAVER;
    LPGBT_Ctrl(iLPGBT).downlink.bypass_encoder     <= ITDTC_IO_Ctrl.LPGBT(iLPGBT+1).DOWNLINK.BYPASS.ENCODER;
    LPGBT_Ctrl(iLPGBT).downlink.bypass_scrambler   <= ITDTC_IO_Ctrl.LPGBT(iLPGBT+1).DOWNLINK.BYPASS.SCRAMBLER;  

    -----------------------------------------------------------------------------
    -- From MGT to user
    -----------------------------------------------------------------------------
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).DOWNLINK.MGT_TX_RST_DONE <= LPGBT_userdata_output(iLPGBT).txresetdone(0);
    ITDTC_IO_Mon.LPGBT(iLPGBT+1).UPLINK.MGT_RX_RST_DONE   <= LPGBT_userdata_output(iLPGBT).rxresetdone(0);
    
  end generate lpgbt_convert;
  
  LPGBT_mapping_1: entity work.LPGBT_mapping
    generic map (
      LPGBT_GENERIC => LPGBT_IP_CONFIGs)
    port map (
      clk_320       => clk_320,
      clk_40en      => clken_40,
      locked_320    => locked_320,
      LPGBT_MGT_up_in  => LPGBT_MGT_up_in,
      LPGBT_MGT_up_out => LPGBT_MGT_up_out,
      LPGBT_MGT_down_in  => LPGBT_MGT_down_in,
      LPGBT_MGT_down_out => LPGBT_MGT_down_out,
      LPGBT_CTRL    => LPGBT_CTRL,
      LPGBT_MON     => LPGBT_MON,
      LPGBT_in      => LPGBT_in,
      LPGBT_out     => LPGBT_out);


  
  
  ITDTC_IO_map_1: entity work.ITDTC_IO_map
    generic map (
      ALLOCATED_MEMORY_RANGE => to_integer(IO_MEM_RANGE))
    port map (
      clk_axi         => axi_clk,
      reset_axi_n     => axi_rst_n,
      slave_readMOSI  => axi_readMOSI(1),
      slave_readMISO  => axi_readMISO(1),
      slave_writeMOSI => axi_writeMOSI(1),
      slave_writeMISO => axi_writeMISO(1),
      Mon             => ITDTC_IO_Mon,
      Ctrl            => ITDTC_IO_Ctrl);
  
end architecture structure;

