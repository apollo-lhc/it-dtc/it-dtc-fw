library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.RD53_Params_PKG.all;

package Trigger_PKG is

  constant BX_ID_SIZE      : integer := 40;
  constant TRIG_FLAG_COUNT : integer := 8;
  constant TRIG_TYPE_COUNT : integer := 2;
  constant FE_BX_ID_SIZE   : integer := 8;
  
  type Trigger_full_t is record
    BX_ID         : std_logic_vector(BX_ID_SIZE-1 downto 0);
    flags         : std_logic_vector(TRIG_FLAG_COUNT-1 downto 0);
  end record Trigger_full_t;

  type FE_Trigger10b_t is record
    tag          : Tag_t;
    tag_BX_mask  : Tag_BX_mask_t;
  end record FE_Trigger10b_t;

  type Trigger10b_t is record
    FE_trig           : FE_Trigger10b_t;
    --trig_id is missing the LS two bits since they can be derived from the FE_trig
    partial_bx_id     : std_logic_vector(FE_BX_ID_SIZE -1 +2 downto 2); 
    BX_mask_DAQ       : Tag_BX_mask_t;
    BX_mask_LUMI      : Tag_BX_mask_t;
    BX_mask_MISSING   : Tag_BX_mask_t;
    
  end record Trigger10b_t;
 
  type EventBuilder_Trigger_t is record
    BX_ID     : std_logic_vector(BX_ID_SIZE-1    downto 0);
    trig_type : std_logic_vector(TRIG_TYPE_COUNT-1 downto 0);
  end record EventBuilder_Trigger_t;
  
end package Trigger_PKG;
