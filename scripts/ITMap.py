#!/usr/bin/env python3
#################################################################################
## Force python3
#################################################################################
import sys
if not sys.version_info.major == 3:
    raise BaseException("Wrong Python version detected.  Please ensure that you are using Python 3.")
#################################################################################

import csv
import argparse


#IsPlusZEnd/O, IsPlusXSide/O, DTC_Id/I, DTC_CMSSW_Id/U, MFB/I, LpGBT_Id/C, LpGBT_CMSSW_IdPerDTC/U, N_ELinks_Per_Module/I, Power_Chain/I, Power_Chain_Type/C, Is_LongBarrel/O, Module_DetId/i, Module_Section/C, Module_Layer/I, Module_Ring/I, Module_phi_deg/D, N_Chips_Per_Module/I, N_Channels_Per_Module/I


#################################################################################
def ParseITDTC_CSV(csvFile):
    with open(csvFile, newline='') as inFile:
#        csvData = csv.reader(inFile,delimiter=' ')
        csvData = csv.DictReader(inFile,delimiter=',',skipinitialspace=True)
        i = 0
        columns=['DTC_Id/I','N_ELinks_Per_Module/I','N_Chips_Per_Module/I']
        lastDTC = -1
        for line in csvData:
            if int(line['DTC_Id/I']) != lastDTC:
                print("%s:" % (line['DTC_Id/I']))
                lastLPGBT = -1
            if int(line['MFB/I']) != lastLPGBT:
                print("  %s:" % (line['MFB/I']))
                lastLPGBT = -1
            
            print("     %s %s" %(line['N_ELinks_Per_Module/I'],line['N_Chips_Per_Module/I']))
            lastDTC = int(line['DTC_Id/I'])
            lastLPGBT = int(line['MFB/I'])

            
    

#################################################################################
## CLI
#################################################################################
if __name__ == "__main__":
    #command line
    parser = argparse.ArgumentParser(description="Parse ITDTC layout files")
    parser.add_argument("--csvFile","-c",help="csv file from https://ghugo.web.cern.ch/ghugo/layouts/cabling/OT616_IT613_CMSSW_Ids/cablingInner.html#cabling")
    args=parser.parse_args()
    ParseITDTC_CSV(csvFile = args.csvFile)
